$(document).ready ->
	update_choice = ->
		url = $(this).attr("data-url")
		choice_id = $(this).attr("data-choice-id")
		question_id = $(this).attr("data-question-id")
		$.ajax(
			url: url
			data:
				answer:
					choice_id: choice_id
					question_id: question_id
			method: "put"
		)
		return false
	$("body").on("change", ".choice-input", update_choice)