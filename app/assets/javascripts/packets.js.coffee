$(document).ready ->
  $("#search_question_input").keydown (e) ->
    if e.keyCode != 13 || $(this).val() == ''
      return true

    $("#question_list").html("<center><i>Loading...</i></center>")
    url = $(this).attr("data-target")
    query = $(this).val()
    $.ajax(
      url: url
      data:
        query: query
      cache: false
    )

  $("#search_packets").keydown (e) ->
    if e.keyCode != 13 || $(this).val() == ''
      return true

    $("#packet_list").html("<center><i>Loading...</i></center>")
    url = $(this).attr("data-target")
    query = $(this).val()
    $.ajax(
      url: url
      data:
        query: query
      cache: false
    )