window.choices = []
window.questionChoicePath = ''
window.choiceScorePath = ''
window.choiceScoresPath = ''

window.createChoices = ->
  if (choices.length == 0)
    $("#choices").html("No choice added. <a class='add-choice' href='#''>Add now</a>")
    return
  
  $("#choices").html("<ul>")
  for choice in choices
    deleteCaption = "<a class='delete-choice' data-choice-id='" + choice.id + "' href='' title='Delete'><i class='glyphicon glyphicon-trash'></i></a>"
    editCaption = "<a class='edit-choice' data-choice-id='" + choice.id + "' href='' title='Edit'><i class='glyphicon glyphicon-edit'></i></a>"
    content = "<b id='choice_" + choice.id + "'>" + choice.content + "</b>"
    editScore = "<a data-choice-id='" + choice.id + "' class='edit-scores' href=''>Edit Scores</a>"
    $("#choices").append("<li style='font-size: 16px'>" + content + " " + deleteCaption + " | " + editCaption + " | " + editScore + "</li>")

  $("#choices").append("</ul>")
  $("#choices").append("<br/><a class='add-choice' href='#''><i class='glyphicon glyphicon-plus'></i> Add more</a>");

$(document).ready ->
  createChoices()
  $("#create_choice_form").ajaxForm()
  $("#update_choice_form").ajaxForm()

  addChoice = ->
    $("#create_choice_form > input[type='text']").val("")
    $(".add-choice").hide();
    $("#create_choice_form").show();
    return false
  $("#choices").on("click", ".add-choice", addChoice)

  editChoice = ->
    id = $(this).attr('data-choice-id')
    content = $("#choice_" + id).html();
    $("#update_choice_form > input[type='text']").val(content)
    $("#update_choice_form").attr("action", questionChoicePath + id).show();
    return false
  $("#choices").on("click", ".edit-choice", editChoice)
  $("#btn_cancel_edit_choice").click ->
    $("#update_choice_form").hide();

  deleteChoiceById = (id) ->
    $.ajax(
      url: questionChoicePath + id
      method: "delete"
      cache:false 
    );

  deleteChoice = ->
    confirmation = confirm("Are you sure?")
    return false if confirmation != true 
    id = $(this).attr("data-choice-id")
    deleteChoiceById(id)
    return false
  $("#choices").on("click", ".delete-choice", deleteChoice)

# Score Editor
$(document).ready ->
  editScores = ->
    id = $(this).attr("data-choice-id")
    url = choiceScoresPath.replace("//", "/" + id + "/")
    $.ajax(
      url:url
      cache:false
    )
    return false
  $("#choices").on("click", ".edit-scores", editScores)

  addToSelectedIntelligence = ->
    choiceId = $(this).attr("data-choice-id")
    intelligenceId = $(this).attr("data-intelligence-id")
    url = choiceScoresPath.replace("//", "/" + choiceId + "/")
    $.ajax(
      url: url
      data:
        intelligence_id: intelligenceId
      method:"post"
      cache:false
    )
    return false
  $("#scores_editor").on("click", ".add-to-selected-intelligence", addToSelectedIntelligence)

  deleteScore = ->
    choiceId = $(this).attr("data-choice-id")
    scoreId = $(this).attr("data-score-id")
    url = choiceScorePath.replace("//", "/" + choiceId + "/") + scoreId
    confirmation = confirm("Are you sure?")
    return false if confirmation != true
    $.ajax(
      url: url
      method:"delete"
      cache:false
    )
    return false  
  $("#scores_editor").on("click", ".delete-score", deleteScore)

  updateScore = ->
    choiceId = $(this).attr("data-choice-id")
    scoreId = $(this).attr("data-score-id")
    url = choiceScorePath.replace("//", "/" + choiceId + "/") + scoreId
    value = $(this).val()
    $(this).attr("readonly", "readonly")
    $.ajax(
      url: url
      method:"put"
      data:
        value: value
      cache:false
    )
    return false  
  $("#scores_editor").on("change", ".score-input", updateScore)