class PacketQuestion < ActiveRecord::Base
  belongs_to :packet
  belongs_to :question
  attr_accessible :packet_id, :question_id
end
