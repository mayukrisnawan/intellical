class Answer < ActiveRecord::Base
  belongs_to :run
  belongs_to :user
  belongs_to :choice
  belongs_to :question
  attr_accessible :run_id, :user_id, :choice_id, :question_id

  def self.find_or_new run_id, question_id, user_id
  	answer = Answer.where("run_id = ? AND question_id = ? AND user_id = ?", run_id, question_id, user_id).first || Answer.new
    answer.run_id = run_id
    answer.question_id = question_id
    answer.user_id = user_id
    answer
  end

  def self.answered? run_id, question_id, user_id, choice_id=nil
    if choice_id != nil
  	   result = Answer.where("run_id = ? AND question_id = ? AND user_id = ? AND choice_id = ?", run_id, question_id, user_id, choice_id).length != 0
    else
      result = Answer.where("run_id = ? AND question_id = ? AND user_id = ?", run_id, question_id, user_id).length != 0
    end

  end
end
