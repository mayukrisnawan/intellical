class Run < ActiveRecord::Base
  belongs_to :packet
  attr_accessible :finish_datetime, :start_datetime, :packet_id, :running

  validates_presence_of :start_datetime
  validates_presence_of :finish_datetime
  validates_presence_of :packet_id

  has_many :answers

  def is_running?
    running
  end
end
