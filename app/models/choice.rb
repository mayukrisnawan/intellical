class Choice < ActiveRecord::Base
  belongs_to :question
  has_many :scores
  attr_accessible :content, :question_id
  validates_presence_of :content
end
