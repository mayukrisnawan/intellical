class Question < ActiveRecord::Base
  attr_accessible :content
  has_many :choices
  has_many :packet_questions
  has_many :packets, :through => :packet_questions
  validates_uniqueness_of :content

  def self.search term
    result = Question.order(:content)
    if term
      term.downcase!
      return result.where("replace(lower(content), '\r\n', ' ') LIKE ?", "%#{term}%")
    end
    return result
  end

  def score_range
    maximum, minimum = false, false
    choices.each do |choice|
      choice.scores.each do |score|
        maximum = score.value if maximum == false || score.value > maximum
        minimum = score.value if minimum == false || score.value < minimum
      end
    end
    return false if maximum == false || minimum == false
    return "from " + minimum.to_s + " to " + maximum.to_s
  end
end
