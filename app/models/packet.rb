class Packet < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :packet_questions
  has_many :questions, :through => :packet_questions

  validates_presence_of :name
  validates_presence_of :description
end
