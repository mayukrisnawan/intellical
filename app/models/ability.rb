class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
    elsif user.questions_creator?
      can :manage, Question
      can :manage, Choice
      can :manage, Score
    elsif user.packets_composer?
      can :manage, Packet
      can :manage, Run
      can :read, Choice
      can :read, Score
      can :read, Question
    elsif user.audience?
      can :read, :all
      can :manage, Answer
    end
  end
end
