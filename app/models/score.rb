class Score < ActiveRecord::Base
  belongs_to :choice
  belongs_to :intelligence
  attr_accessible :value, :choice_id, :intelligence_id
end
