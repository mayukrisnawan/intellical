class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :email_regexp =>  /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i

  attr_accessible :email, :password, :password_confirmation, :remember_me

  has_many :user_roles
  has_many :roles, :through => :user_roles

  after_create :add_audience_role

  def add_audience_role
    add_role 1
  end 

  def add_role role_id
    return false if UserRole.where("role_id = ? AND user_id = ?", role_id, id).length != 0
    UserRole.create({ :role_id => role_id, :user_id => id })
  end

  def delete_role role_id
    UserRole.where("role_id = ? AND user_id = ?", role_id, id).first.delete
  end

  # 1 => audience
  # 2 => admin
  # 3 => questions_creator
  # 4 => packets_composer

  def audience?
    current_roles = roles.map(&:name).select {|role| role == "audience" }
    current_roles.length != 0
  end

  def admin?
    current_roles = roles.map(&:name).select {|role| role == "admin" }
    current_roles.length != 0
  end

  def questions_creator?
    current_roles = roles.map(&:name).select {|role| role == "questions_creator" }
    current_roles.length != 0
  end

  def packets_composer?
    current_roles = roles.map(&:name).select {|role| role == "packets_composer" }
    current_roles.length != 0
  end
end
