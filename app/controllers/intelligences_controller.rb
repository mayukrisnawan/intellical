class IntelligencesController < ApplicationController
  load_and_authorize_resource
  def index
    @intelligences = Intelligence.order(:name).all
  end

  def new
    @intelligence = Intelligence.new
  end

  def edit
    @intelligence = Intelligence.find(params[:id])
  end

  def create
    @intelligence = Intelligence.new(params[:intelligence])
    response = @intelligence.save ? "create_success" : "create_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def update
    @intelligence = Intelligence.find(params[:id])
    response = @intelligence.update_attributes(params[:intelligence]) ? "update_success" : "update_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def destroy
    @intelligence = Intelligence.find(params[:id])
    response = @intelligence.delete ? "destroy_success" : "destroy_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end
end