class UsersController < ApplicationController
  load_and_authorize_resource
  def index
    @users = User.order(:email)
                 .paginate(:per_page => 10, :page => params[:page])
  end

  def add_role
    @user = User.find(params[:id])
    if @user.roles.length == 0
      @roles = Role.all
    else
      @roles = Role.where("id NOT IN (?)", @user.roles.map(&:id))
    end
    respond_to do |format|
      format.js
    end
  end

  def destroy_role
    @user = User.find(params[:id])
    @role = Role.find(params[:role_id])
    @user.delete_role @role.id
    respond_to do |format|
      format.js
    end
  end

  def create_role
    @user = User.find(params[:id])
    @user.add_role params[:role_id]
    respond_to do |format|
      format.js
    end
  end
end