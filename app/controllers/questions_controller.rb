class QuestionsController < ApplicationController
  load_and_authorize_resource
  def index
    @questions = Question.search(params[:search])
                         .paginate(:per_page => 10, :page => params[:page])
  end

  def show
    @question = Question.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  def new
    @question = Question.new
  end

  def edit
    @question = Question.find(params[:id])
  end

  def create
    @question = Question.new(params[:question])
    response = @question.save ? "create_success" : "create_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def update
    @question = Question.find(params[:id])
    response = @question.update_attributes(params[:question]) ? "update_success" : "update_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def destroy
    @question = Question.find(params[:id])
    response = @question.delete ? "destroy_success" : "destroy_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end
end