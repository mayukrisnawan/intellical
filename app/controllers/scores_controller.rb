class ScoresController < ApplicationController
  load_and_authorize_resource
  def index
    @choice = Choice.find(params[:choice_id])
    @scores = Score.where(:choice_id => @choice.id)
    if @scores.size != 0
      @intelligences = Intelligence.order(:name).where("id NOT IN (?)", @scores.map{ |score| score.intelligence_id })
    else
      @intelligences = Intelligence.order(:name).all
    end
    respond_to do |format|
      format.js
    end
  end

  def create
    @choice = Choice.find(params[:choice_id])
    @intelligence = Intelligence.find(params[:intelligence_id])
    @score = Score.new(:choice_id => params[:choice_id], :intelligence_id => params[:intelligence_id])
    @score.value = 0
    response = @score.save ? "create_success" : "create_failed"
    respond_to do |format|
      format.js { render response }
    end
  end

  def update
    @choice = Choice.find(params[:choice_id])
    @score = Score.find(params[:id])
    @score.value = params[:value] || 0 
    response = @score.save ? "update_success" : "update_failed"
    respond_to do |format|
      format.js { render response }
    end
  end

  def destroy
    @choice = Choice.find(params[:choice_id])
    @score = Score.find(params[:id])
    @intelligence = @score.intelligence
    response = @score.delete ? "destroy_success" : "destroy_failed"
    respond_to do |format|
      format.js { render response }
    end
  end
end