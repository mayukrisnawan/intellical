class AnswersController < ApplicationController
	load_and_authorize_resource
	def index
		@run = Run.find(params[:run_id])
		@answers = Answer.where("run_id = ? AND user_id=?", @run.id, current_user.id)
	end

  def update_answer
    @run = Run.find(params[:run_id])
    @question = Question.find(params[:answer][:question_id])
    @answer = Answer.find_or_new(params[:run_id], params[:answer][:question_id], current_user.id)
    @answer.choice_id = params[:answer][:choice_id]
    puts ">>>>>" + @answer.inspect
    response = @answer.save ? "update_success" : "update_failed"
    respond_to do |format|
      format.js { render response }
    end
  end

  def statistic
    @run = Run.find(params[:run_id])
    @answers = Answer.where("run_id = ? AND user_id=?", @run.id, current_user.id)

    @choice = @run.answers.map {|answer| answer.choice }
    @data = {}
    @choice.each do |choice|
      choice.scores.each do |score|
        intelligence = score.intelligence.name
        if @data[intelligence] == nil
          @data[intelligence] = score.value
        else
          @data[intelligence] += score.value
        end
      end
    end

    @chart_source = []
    @data.each do |key, value|
      @chart_source << { :label => key, :value => value }
    end
  end
end