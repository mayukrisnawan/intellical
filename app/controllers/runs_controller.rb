class RunsController < ApplicationController
  load_and_authorize_resource
  def index
    if can? :manage, Run
      @runs = Run.order("start_datetime desc").all
    else
      @runs = Run.order("start_datetime desc").where(:running => true)
    end
  end

  def new
    @run = Run.new
  end

  def create
    @run = Run.new(params[:run])
    # puts ">>>>>" + @run.inspect
    response = @run.save ? "create_success" : "create_failed"
    respond_to do |format|
      format.js { render response }
    end
  end

  def destroy
    @run = Run.find(params[:id])
    response = @run.delete ? "destroy_success" : "destroy_failed"
    respond_to do |format|
      format.js { render response }
    end
  end

  def input_date
    @packet = Packet.find(params[:packet_id])
    @run = Run.new
    @run.packet_id = @packet.id
    respond_to do |format|
      format.js
    end
  end

  def start_run
    @run = Run.find(params[:id])
    @run.running = true
    response = @run.save ? "start_run_success" : "start_run_failed"
    respond_to do |format|
      format.js { render response }
    end
  end

  def stop_run
    @run = Run.find(params[:id])
    @run.running = false
    response = @run.save ? "stop_run_success" : "stop_run_failed"
    respond_to do |format|
      format.js { render response }
    end
  end
end