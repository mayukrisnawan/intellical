class ChoicesController < ApplicationController
  load_and_authorize_resource
  def index
    @question = Question.find(params[:question_id])
    @choices = @question.choices.order(:content)
    @choice = Choice.new
  end

  def create
    @question = Question.find(params[:question_id])
    @choice = Choice.new(params[:choice])
    @choice.question_id = @question.id
    response = @choice.save ? "create_success" : "create_failed"

    respond_to do |format|
      format.js { render response }
    end
  end

  def update
    @question = Question.find(params[:question_id])
    @choice = Choice.find(params[:id])
    response = @choice.update_attributes(params[:choice]) ? "update_success" : "update_failed"
    @choices = @question.choices.sort

    respond_to do |format|
      format.js { render response }
    end
  end

  def destroy
    @question = Question.find(params[:question_id])
    @choice = Choice.find(params[:id])
    response = @choice.delete ? "destroy_success" : "destroy_failed"

    respond_to do |format|
      format.js { render response }
    end
  end
end