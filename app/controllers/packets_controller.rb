class PacketsController < ApplicationController
  load_and_authorize_resource
  def index
    @packets = Packet.all
  end

  def search
    @query = (params[:query] ? params[:query] : '').downcase
    @packets = Packet.order(:name).where("lower(name) LIKE ?", "%#{@query}%")

    respond_to do |format|
      format.js { render "packets/search" }
    end
  end

  def new
    @packet = Packet.new
  end

  def edit
    @packet = Packet.find(params[:id])
  end

  def create
    @packet = Packet.new(params[:packet])
    response = @packet.save ? "create_success" : "create_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def update
    @packet = Packet.find(params[:id])
    response = @packet.update_attributes(params[:packet]) ? "update_success" : "update_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def destroy
    @packet = Packet.find(params[:id])
    response = @packet.delete ? "destroy_success" : "destroy_failed"
    respond_to do |format|
      format.js { render response }      
    end
  end

  def questions_index
    @packet = Packet.find(params[:id])
    respond_to do |format|
      format.html { render "packets/questions/index" }
    end
  end

  def questions_search
    @packet = Packet.find(params[:id])
    @query = (params[:query] ? params[:query] : '%%').downcase
    if @packet.questions.length == 0
      @questions = Question.order(:content).where("lower(content) LIKE ?", "%#{@query}%")
    else
      @questions = Question.order(:content).where("id NOT IN (?) AND lower(content) LIKE ?", @packet.questions , "%#{@query}%")
    end

    respond_to do |format|
      format.js { render "packets/questions/questions_search" }
    end
  end

  def add_question
    @packet = Packet.find(params[:id])
    @packet_question = PacketQuestion.new({ :question_id => params[:question_id], :packet_id => params[:id] })
    response =  @packet_question.save ? "add_question_success" : "add_question_failed"
    response = "packets/questions/" + response
    respond_to do |format|
      format.js { render response }
    end
  end

  def destroy_question
    @packet = Packet.find(params[:id])
    @packet_question = PacketQuestion.where({ :question_id => params[:question_id], :packet_id => params[:id] }).first
    response =  @packet_question.delete ? "destroy_question_success" : "destroy_question_failed"
    response = "packets/questions/" + response
    respond_to do |format|
      format.js { render response }
    end
  end
end