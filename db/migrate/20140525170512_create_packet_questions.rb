class CreatePacketQuestions < ActiveRecord::Migration
  def change
    create_table :packet_questions do |t|
      t.references :packet
      t.references :question

      t.timestamps
    end
    add_index :packet_questions, :packet_id
    add_index :packet_questions, :question_id
  end
end
