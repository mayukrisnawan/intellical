class AddStatusToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :running, :boolean, :default => false
  end
end
