class AddDatetimePrefixToRuns < ActiveRecord::Migration
  def self.up
    rename_column :runs, :start, :start_datetime
    rename_column :runs, :finish, :finish_datetime
  end
  def self.down
    rename_column :runs, :start_datetime, :start
    rename_column :runs, :finish_datetime, :finish
  end
end
