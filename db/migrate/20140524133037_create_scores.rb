class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.references :choice
      t.references :intelligence
      t.integer :value

      t.timestamps
    end
    add_index :scores, :choice_id
    add_index :scores, :intelligence_id
  end
end
