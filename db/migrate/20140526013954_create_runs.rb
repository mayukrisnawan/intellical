class CreateRuns < ActiveRecord::Migration
  def change
    create_table :runs do |t|
      t.references :packet
      t.datetime :start
      t.datetime :finish

      t.timestamps
    end
    add_index :runs, :packet_id
  end
end
