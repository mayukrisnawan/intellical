class CreatePackets < ActiveRecord::Migration
  def change
    create_table :packets do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
