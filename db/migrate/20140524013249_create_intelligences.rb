class CreateIntelligences < ActiveRecord::Migration
  def change
    create_table :intelligences do |t|
      t.string :name

      t.timestamps
    end
  end
end
