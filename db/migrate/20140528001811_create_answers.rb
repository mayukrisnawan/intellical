class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.references :run
      t.references :user
      t.references :question
      t.references :choice

      t.timestamps
    end
    add_index :answers, :run_id
    add_index :answers, :user_id
    add_index :answers, :question_id
    add_index :answers, :choice_id
  end
end
