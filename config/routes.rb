Intellical::Application.routes.draw do
  devise_for :users

  root :to => "guest#index"

  #admin routes
  # get "admin/" => "admin#index", :as => "admin_index"

  resources :packets do
    collection do
      get "search" => "packets#search"
    end
    member do
      get "questions" => "packets#questions_index"
      get "questions_search" => "packets#questions_search"
      post "add_question/:question_id" => "packets#add_question", :as => "add_question"
      delete "destroy_question/:question_id" => "packets#destroy_question", :as => "destroy_question"
    end
  end

  resources :questions do
    resources :choices
  end
  resources :intelligences

  resources :choices, :only =>[] do
    resources :scores
  end

  resources :runs do
    collection do
      get "input_date/:packet_id" => "runs#input_date", :as => "input_date"
    end
    member do
      put "start_run" => "runs#start_run", :as => "start"
      put "stop_run" => "runs#stop_run", :as => "stop"
    end
    resources :answers do
      collection do
        put "update_answer/:question_id" => "answers#update_answer", :as => "update_answer"
        get "statistic" => "answers#statistic", :as => "statistic"
        get "chart" => "answers#chart", :as => "chart"
      end
    end
  end

  resources :users do
    member do
      get "add_role" => "users#add_role"
      post "create_role" => "users#create_role"
      delete "destroy_role/:role_id" => "users#destroy_role", :as => "destroy_role"
    end
  end
end
